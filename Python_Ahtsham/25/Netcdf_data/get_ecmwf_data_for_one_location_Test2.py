# -*- coding: utf-8 -*-
"""
Created on Tue Aug 21 13:40:19 2018

@author: mrazi
"""

'''
Created on Aug 20, 2018

@author: sdoerner
'''
# Requires packages netCDF4 and scipy
from datetime import datetime
from netCDF4 import Dataset
from scipy.interpolate import interpn
import math
from math import atan2
import numpy as np
import pandas as pd
import csv

#######Interpolation coordinates:
##Lahore_Valencia_2018_II
lon = 74.2512
lat = 31.4001


def readnetCDF(path_to_final,hours):
    ecmwf_data = {}
    with Dataset(path_to_final) as data:
        ecmwf_vars = data.variables
        #######Flip latitudes for interpolation
        ecmwf_data['lat'] = ecmwf_vars['latitude'][::-1]
        ecmwf_data['lon'] = ecmwf_vars['longitude'][:]
        ######Read 2D variables and flip axes for later interpolation
        ecmwf_data['10u'] = ecmwf_vars['10u'][::-1, :].swapaxes(0, 1)
        ecmwf_data['10v'] = ecmwf_vars['10v'][::-1, :].swapaxes(0, 1)
        #####For altitude variables: derive altitude above ground (hcal - sfcalt)
        tmp_sfc = ecmwf_vars['sfcalt'][::-1, :]
        tmp_h = ecmwf_vars['hcal'][::-1, ::-1, :]
        tmp_h -= tmp_sfc  # Comment this line if you want absolute altitude a.s.l.
        ##### Read 3D variables
        ecmwf_data['u'] = ecmwf_vars['u'][::-1, ::-1, :].swapaxes(0, 2)
        ecmwf_data['v'] = ecmwf_vars['v'][::-1, ::-1, :].swapaxes(0, 2)
        ecmwf_data['h'] = tmp_h.swapaxes(0, 2)

    ##### Finally interpolate the dataset to a specific location
    loc_data = {}
    for key in ['10u', '10v', 'u', 'v', 'h']:
        loc_data[key] = interpn((ecmwf_data['lon'], ecmwf_data['lat'],),
                                ecmwf_data[key], (lon, lat))
        loc_data[key] = loc_data[key].squeeze()

    loc_data['hours'] = hours
    dataframe = pd.DataFrame.from_dict(loc_data)
    return dataframe


def calculateMetrics(path_to_final):
        loc_data = {}
        read_data = pd.DataFrame

        var1 =0
        var2 =0
        FinalParam25 = pd.DataFrame()
        datafile = pd.read_csv(path_to_final)
        for i in range(4):
            row =datafile.iloc[[i* 139]]
            var1 = float(row['10u'])
            var2 = float(row['10v'])


            #loc_data = ['DirGT','DirCF','Speed']
            # calculate wind direction going towards
            loc_data['DirGT'] = (((math.atan2(var1, var2)) * 180 / np.pi) % 360)
            #calculate wind direction coming from
            loc_data['DirCF'] = (((math.atan2(var1,var2))*180/np.pi)+180)
            #calculate wind speed
            loc_data['Speed'] = math.sqrt(int(pow(var1, 2) + pow(var2, 2)))

            FinalParam25=FinalParam25.append(loc_data,ignore_index=True)



        return FinalParam25


if __name__ == '__main__':
    pathtofile= 'D:\pythonProjects\Python_Ahtsham\Python_Ahtsham\\25\\Netcdf_data\\netcdf_complete\\'

    # data processing of 25
    path_to_File = pathtofile+'20180625_00_complete.nc'
    Datafile =readnetCDF(path_to_File, '00')
    pathtonewfile = pathtofile+'20180625_06_complete.nc'
    Datafile = Datafile.append(readnetCDF(pathtonewfile, '06'))
    pathtonewfile = pathtofile+'20180625_12_complete.nc'
    Datafile = Datafile.append(readnetCDF(pathtonewfile, '12'))
    pathtonewfile = pathtofile+'20180625_18_complete.nc'
    Datafile = Datafile.append(readnetCDF(pathtonewfile, '18'))

    Datafile.to_csv('CDFFinalData25.csv')
    #print(Datafile)
    Datafile.iloc[0:0]


    # data processing of 26

    pathtonewfile = pathtofile+'20180626_00_complete.nc'
    Datafile1 = Datafile.append(readnetCDF(pathtonewfile, '00'))
    pathtonewfile = pathtofile+'20180626_06_complete.nc'
    Datafile1 = Datafile.append(readnetCDF(pathtonewfile, '06'))
    pathtonewfile = pathtofile+'20180626_12_complete.nc'
    Datafile1 = Datafile.append(readnetCDF(pathtonewfile, '12'))
    pathtonewfile = pathtofile+'20180626_18_complete.nc'
    Datafile1 = Datafile.append(readnetCDF(pathtonewfile, '18'))

    Datafile1.to_csv('CDFFinalData26.csv')

#eexport final values of speed and direction
    path_to_File ='CDFFinalData25.csv'
    df = calculateMetrics(path_to_File)
    df.to_csv('FInalParam25.csv')


    path_to_File ='CDFFinalData26.csv'
    df1 = calculateMetrics(path_to_File)
    df.to_csv('FInalParam26.csv')