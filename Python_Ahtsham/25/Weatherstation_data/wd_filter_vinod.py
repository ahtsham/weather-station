# -*- coding: utf-8 -*-
"""
Created on Tue Nov  6 22:45:46 2018

@author: Vinod
"""
####This script slice data for times mentioned in lines 21 to 23 and then calculate mean wind speed and direction Wsavg_WVc(1) and Wsavg_WVc(3)in columns 5 and 7 of file 
import numpy as np
import pandas as pd

df = pd.read_csv(r'Z:\nobackup\donner\forMaria\MetData_2018\\metdata_2018_II_Lahore\\\Valencia_Average_org.dat',delimiter=',',\
                   skiprows = [0,2,3], dtype = None)
col_names = df.columns
col_names = col_names.insert(len(col_names),"ws_avg")
col_names = col_names.insert(len(col_names),"wd_max")
df['date_time'] = pd.to_datetime(df['TIMESTAMP'], format='%Y-%m-%d %H:%M:%S')
df['hours'] = [dt.hour for dt in df['date_time']]
df['mins'] = [dt.minute for dt in df['date_time']]
df['hour_min'] = [str(h)+':'+str(m) for h,m in zip(df['hours'],df['mins'])] 
df2 = df[df['hour_min'].isin(["23:55", "0:0", "0:5",\
         "5:55", "6:0", "6:5",\
         "11:55", "12:0", "12:5",\
         "17:55", "18:0", "18:5"])]

        # df2['wd_max'] = np.nan
###here instead of maximum value of 3 , I need to calculate mean wind direction described
#in 'read_and_calculate_wind_speed_direction_u_v_weatherstations_for_ahtesham.py'from line 47 to end.
#wind_dir is Wsavg_WVc(3) and wind_speed is Wsavg_WVc(1)
df2['ws_avg'] = np.nan  ## here also calculate  mean wind speed i.e mspeed by formula given in script mentioned in line 26

for idx in df2.index:
    if df2['mins'][idx] == 0:
        rel_ws = [df2['Wsavg_WVc(1Wsavg_WVc(3))'][idx], df2['Wsavg_WVc(1)'][idx-1], df2['Wsavg_WVc(1)'][idx+1]]
        rel_wd = [df2['Wsavg_WVc(3)'][idx], df2['Wsavg_WVc(3)'][idx-1], df2['Wsavg_WVc(3)'][idx+1]]
        df2['ws_avg'][idx] = np.mean(rel_ws)
        df2['wd_max'][idx] = np.max(rel_wd)
df3 = df2[df2['hour_min'].isin(["0:0", "6:0", "12:0", "18:0"])][col_names]
df3.to_csv(r'M:\Python_Sebastian\Wind_data_scripts\Valencia_Average_wd_test.dat', sep = '\t', index=False)
    
#df['hour_min'] = [h+(m/60) for h,m in zip(df['hours'],df['mins'])] 