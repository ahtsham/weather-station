import pandas as pd
import csv

pathNCF25= 'D:\pythonProjects\Python_Ahtsham\Python_Ahtsham\\25\\Netcdf_data\FInalParam25.csv'
pathNCF26= 'D:\pythonProjects\Python_Ahtsham\Python_Ahtsham\\25\\Netcdf_data\FInalParam26.csv'

cdfData25 = pd.read_csv(pathNCF25)
cdfData26 = pd.read_csv(pathNCF26)
cdfdirecdata25 = cdfData25['DirCF']
cdfdirecata26 = cdfData26['DirCF']
cdfdirecdata=[]
cdfdirecdata += list(cdfdirecdata25)
cdfdirecdata += list(cdfdirecata26)
print(cdfdirecdata)

wsData = pd.read_csv('D:\pythonProjects\Python_Ahtsham\Python_Ahtsham\\25\Weatherstation_data\\finalValues.csv', usecols=['DirecCF'])
wsData = wsData['DirecCF'][21:29]
print(wsData)
import  numpy as np
import matplotlib as mt
# np.corrcoef(cdfspeeddata,wsData)
import matplotlib.pyplot as plt

# mt.matshow(dataframe.corr())


df = pd.DataFrame()

df['cdf']  = pd.Series(cdfdirecdata)
df['ws'] = wsData.values


print(df.corr())

import seaborn as sns

import matplotlib.pyplot as plt
# matplotlib.style.use('ggplot')

plt.imshow(df.corr(), cmap=plt.cm.Reds, interpolation='nearest')
plt.colorbar()
tick_marks = [i for i in range(len(df.columns))]
plt.xticks(tick_marks, df.columns, rotation='vertical')
plt.yticks(tick_marks, df.columns)
plt.show()
