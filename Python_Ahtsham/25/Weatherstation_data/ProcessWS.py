import csv
import pandas as pd
import math
import numpy as np


def calculateU(speed,windirection):
    theta = (((450 - windirection) % 360) + 180)  ##calculation of theta for u and v calculation
    u = (speed * (np.cos(np.pi / 180 * (theta))) ) ##calculation of u
    return u

def calculateV(speed,windirection):
    theta = (((450 - windirection) % 360) + 180)  ##calculation of theta for u and v calculation
    v = (speed* (np.sin(np.pi / 180 * (theta))) ) ##calculation of v
    return v

data = pd.read_csv('data.dat', sep=',', usecols=['TIMESTAMP', 'speed', 'direction'])
final_data = pd.DataFrame()

final_data = final_data.iloc[0:0]

initial_rows = [0, 1, 2]
initail_data_rows = data.iloc[initial_rows]

first_row = {}
first_row['TIMESTAMP'] = initail_data_rows['TIMESTAMP'][1]
first_row['speed'] = initail_data_rows['speed'].mean()
first_row['direction'] = initail_data_rows['direction'].mean()

final_data = final_data.append(first_row, ignore_index=True)

final_Values=pd.DataFrame()

#each iteration takes three rows for each time
for row in range(1, int(data.__len__() / 72) + 1):
    local_df = data.iloc[range(row * 72 , row * 72 + 5)]

    print(local_df)
    v={}
    u={}
    local_row = {}
    TIMESTAMP = str([local_df.TIMESTAMP][0]).split("    ")[3].split("\n")[0]

    v1 = float(str([local_df['speed']][0]).split('    ')[1].split('\n')[0])
    v2= float(str([local_df['speed']][0]).split('    ')[2].split('\n')[0])
    v3=float(str([local_df['speed']][0]).split('    ')[3].split('\n')[0])

    u1 = float(str([local_df['direction']][0]).split('    ')[1].split('\n')[0])
    u2 = float(str([local_df['direction']][0]).split('    ')[2].split('\n')[0])
    u3 = float(str([local_df['direction']][0]).split('    ')[3].split('\n')[0])

    v[0] = calculateV(v1,u1)
    v[1] = calculateV(v2, u2)
    v[2] = calculateV(v3, u3)
    u[0] = calculateU(v1, u1)
    u[1] = calculateU(v2, u2)
    u[2] = calculateU(v3, u3)

    #Calculate Mean for each time using 3 rows
    Vmean = float(sum(v.values())) / len(v)
    Umean = float(sum(u.values())) / len(u)

    #Calculation of speed, direcrtion
    Wind_Speed_calculated = np.sqrt(Umean ** 2 + Vmean** 2)
    ###calculate  wind direction going towards
    Wind_direction_gt = ((math.atan2(Umean, Vmean)) * 180 / np.pi) % 360
    Wind_direction_cf = ((math.atan2(Umean, Vmean)) * 180 / np.pi) + 180
    final_Values = final_Values.append({'TIMESTAMP':TIMESTAMP, 'DirecCF': Wind_direction_cf, 'DirecGT': Wind_direction_gt, 'Speed':Wind_Speed_calculated}, ignore_index=True)

final_Values.to_csv('finalValues.csv')




