import pandas as pd
import  numpy as np
import matplotlib as mt
import csv
path = 'D:\pythonProjects\Python_Ahtsham\Python_Ahtsham\\25\\Netcdf_data\\'

pathNCF25= 'D:\pythonProjects\Python_Ahtsham\Python_Ahtsham\\25\\Netcdf_data\FInalParam25.csv'
pathNCF26= 'D:\pythonProjects\Python_Ahtsham\Python_Ahtsham\\25\\Netcdf_data\FInalParam26.csv'

cdfData25 = pd.read_csv(pathNCF25)
cdfData26 = pd.read_csv(pathNCF26)
cdfspeedata25 = cdfData25['Speed']
cdfspeedata26 = cdfData26['Speed']
cdfspeeddata=[]
cdfspeeddata += list(cdfspeedata25)
cdfspeeddata += list(cdfspeedata26)
print(cdfspeeddata)

wsData = pd.read_csv('D:\pythonProjects\Python_Ahtsham\Python_Ahtsham\\25\Weatherstation_data\\finalValues.csv', usecols=['Speed'])
wsData = wsData['Speed'][21:29]
# print(wsData)

# np.corrcoef(cdfspeeddata,wsData)
import matplotlib.pyplot as plt

# mt.matshow(dataframe.corr())


df = pd.DataFrame()

df['cdf']  = pd.Series(cdfspeeddata)
df['ws'] = wsData.values

#
# print(df.corr())
#
# import seaborn as sns
#
# import matplotlib.pyplot as plt
# # matplotlib.style.use('ggplot')
#
# plt.imshow(df.corr(), cmap=plt.cm.Reds, interpolation='nearest')
# plt.colorbar()
# tick_marks = [i for i in range(len(df.columns))]
# plt.xticks(tick_marks, df.columns, rotation='vertical')
# plt.yticks(tick_marks, df.columns)
# plt.show()




import csv
import pandas as pd
import math
import numpy as np

no2_dscds = []
so2_dscds = []


no2_dscds = np.array(cdfspeeddata)
so2_dscds = np.array(wsData.values)

print(no2_dscds)
print(so2_dscds)

# do the correlation plots

# SO2 against NO2
fig = plt.figure(figsize=(5.511811024, 4.133858268))
ax1 = fig.add_subplot(1, 1, 1)
fitCols = np.arange(0, 6.e16, 1.e16)  ###########for 16 may and 6th june
# fitCols = np.arange(-2.e16, 3e17, 1.e16)
m, b = np.polyfit(no2_dscds, so2_dscds, 1)
r = np.corrcoef(no2_dscds, so2_dscds)
r2 = r ** 2
fitLine = b + m * fitCols
ax1.plot(no2_dscds / 1.e16, so2_dscds / 1.e16, '.', ms=1)
ax1.set_xlabel('$NO_2$ DSCD $[x10^{16} molec/cm^2]$')
ax1.set_ylabel('$SO_2$ DSCD $[x10^{16} molec/cm^2]$')
ax1.plot(fitCols / 1.e16, fitLine / 1.e16, 'r-', lw=0.75)
ax1.text(0.5, 0.96, 'Slope = %1.2f' % m, transform=ax1.transAxes, fontsize=8, color='red')
ax1.text(0.5, 0.91, 'Intercept = %1.2e $molec/cm^2$' % b, transform=ax1.transAxes, fontsize=8, color='red')
ax1.text(0.5, 0.86, '$R^2$ = %1.2f' % r2[0, 1], transform=ax1.transAxes, fontsize=8, color='red')
ax1.grid(True)
plt.savefig(path + 'correlation_SO2_NO2_' +'.png', dpi=300, bbox_inches='tight')
plt.show()