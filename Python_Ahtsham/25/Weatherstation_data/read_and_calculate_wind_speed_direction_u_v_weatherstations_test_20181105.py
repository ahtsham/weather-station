# -*- coding: utf-8 -*-
"""
Created on Mon Sep 03 14:19:35 2018

@author: mrazi
"""
###### this code reads and calculate wind speed and direction as well as u and v components from weatherstation measured data.
import math
from math import atan2 
import numpy as np
from math import cos
from math import sin
import numpy as np
import matplotlib.pyplot as plt
import os
import matplotlib as mpl
import datetime
import matplotlib.dates as mdates


# sets the font in the plots
mpl.rcParams.update({
    'text.usetex': False,
    'font.family': 'Times New Roman',
    'font.size': 10,
    'legend.fontsize': 8,
    'mathtext.default': 'regular'})

# define paths etc.
base_path = 'D:\pythonProjects\Python_Ahtsham\Python_Ahtsham\25\Weatherstation_data\\'
data_file = base_path + 'Valencia_Average.dat'
out_path = base_path
#time_offset = datetime.timedelta(hours=5)

# read the data file and parse time
data = np.genfromtxt(data_file, dtype='S19,f,f,f,f,f', delimiter=',', skip_header=1,
                     usecols=[0, 2, 3, 4, 5, 7], names=['date', 'Temp', 'RH', 'pressure', 'wind_speed', 'wind_dir'])
times = np.array([datetime.datetime.strptime(data['date'][idx], '%Y-%m-%d %H:%M:%S') for idx in range(len(data['date']))])

print ('reading done')

#print 'windspeed_file', data['wind_speed']
#print data['wind_dir']
#******************************************************************************************************************
######Calculte u and v components to be later used in persistence and mean wind direction calculation########

theta = ((450-(data['wind_dir'])) % 360)+180      ##calculation of theta for u and v calculation

#print 'theta', theta

u = (data['wind_speed'])*(np.cos(np.pi/180*(theta)))                   ##calculation of u
#
v = (data['wind_speed'])*(np.sin(np.pi/180*(theta)))                    ##calculation of v
#***************************************************************************************************
#calculate mean u and v for persistence and mean wdirection calculation

mx=np.mean([u])  
my=np.mean([v])  
#print 'mx =' , mx
#print 'my =' , my 

#********************************************************************************************************************************
mspeed=np.mean(data['wind_speed'])  ##calculation of mean wind speed (from wstation's output values) for persistence calculation

mv= np.sqrt(mx**2 +my**2)           ##this mv variable is needed to calculate persistence

Wind_Speed_calculated = np.sqrt(u**2 + v**2) ####wind speed calculated by using u and v components
#print 'wind speed calculated' , Wind_Speed_calculated

#*********************************************************************
###calculate persistence#######
persistence= mv/mspeed
#print 'persistence', persistence

#**********************************************************************
###calculate mean wind direction from conny formula (e.g 270 is <------------ and 90 is --------> ) ######## 
mangle=(np.arctan2(mx,my)*180/np.pi)
if mx<0:
    mangle=360+mangle
print ( 'mean wind angle', mangle)
##*********************************************************

###calculate mean wind direction going towards 
Wind_direction_gt = ((math.atan2(mx,my))*180/np.pi)%360
print ('direction going to is' , Wind_direction_gt)
##

####calculate wind direction coming from
Wind_direction_cf = ((math.atan2(mx,my))*180/np.pi)+180
print ('direction coming from is' , Wind_direction_cf)


