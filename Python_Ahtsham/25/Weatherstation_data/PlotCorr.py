import csv
import pandas as pd
import math
import numpy as np

no2_dscds = []
so2_dscds = []

for date in dates:
    cur_fileName = date + '_NO2_SO2_gly_hcho.txt'
    data = np.genfromtxt(data_path + cur_fileName, names=True, delimiter='\t')

no2_dscds = np.array(no2_dscds)
so2_dscds = np.array(so2_dscds)

print
'reading done'
# do the correlation plots

# SO2 against NO2
fig = plt.figure(figsize=(5.511811024, 4.133858268))
ax1 = fig.add_subplot(1, 1, 1)
fitCols = np.arange(0, 6.e16, 1.e16)  ###########for 16 may and 6th june
# fitCols = np.arange(-2.e16, 3e17, 1.e16)
m, b = np.polyfit(no2_dscds, so2_dscds, 1)
r = np.corrcoef(no2_dscds, so2_dscds)
r2 = r ** 2
fitLine = b + m * fitCols
ax1.plot(no2_dscds / 1.e16, so2_dscds / 1.e16, '.', ms=1)
ax1.set_xlabel('$NO_2$ DSCD $[x10^{16} molec/cm^2]$')
ax1.set_ylabel('$SO_2$ DSCD $[x10^{16} molec/cm^2]$')
ax1.plot(fitCols / 1.e16, fitLine / 1.e16, 'r-', lw=0.75)
ax1.text(0.5, 0.96, 'Slope = %1.2f' % m, transform=ax1.transAxes, fontsize=8, color='red')
ax1.text(0.5, 0.91, 'Intercept = %1.2e $molec/cm^2$' % b, transform=ax1.transAxes, fontsize=8, color='red')
ax1.text(0.5, 0.86, '$R^2$ = %1.2f' % r2[0, 1], transform=ax1.transAxes, fontsize=8, color='red')
ax1.grid(True)
plt.savefig(out_path + 'correlation_SO2_NO2_' + date + '.png', dpi=300, bbox_inches='tight')
plt.show()